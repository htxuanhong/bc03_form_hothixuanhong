import {
  ADD_STUDENT_LIST,
  DELETE_STUDENT,
  EDIT_STUDENT,
  SELECT_STUDENT,
} from "../constants/constant";

var dsnvJson = localStorage.getItem("DSSV_LOCALSTORAGE");

let initialState = {
  danhSachSinhVien: JSON.parse(dsnvJson) || [],
  selectedStudent: null,
};

export const formReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_STUDENT_LIST: {
      let cloneDanhSach = [...state.danhSachSinhVien];

      cloneDanhSach.push(payload);

      localStorage.setItem("DSSV_LOCALSTORAGE", JSON.stringify(cloneDanhSach));

      return {
        ...state,
        danhSachSinhVien: cloneDanhSach,
      };
    }
    case DELETE_STUDENT: {
      let cloneDanhSach = [...state.danhSachSinhVien];
      let index = cloneDanhSach.findIndex((item) => {
        return item.maSV === payload.maSV;
      });

      cloneDanhSach.splice(index, 1);

      localStorage.setItem("DSSV_LOCALSTORAGE", JSON.stringify(cloneDanhSach));

      return { ...state, danhSachSinhVien: cloneDanhSach };
    }

    case EDIT_STUDENT: {
      let cloneDanhSach = [...state.danhSachSinhVien];
      let index = cloneDanhSach.findIndex((item) => {
        return item.maSV === payload.maSV;
      });

      cloneDanhSach[index] = payload;

      localStorage.setItem("DSSV_LOCALSTORAGE", JSON.stringify(cloneDanhSach));

      return { ...state, danhSachSinhVien: cloneDanhSach };
    }

    case SELECT_STUDENT:
      return { ...state, selectedStudent: payload };

    case "REMOVE_SELECTED_STUDENT":
      return { ...state, selectedStudent: null };

    default:
      return state;
  }
};
