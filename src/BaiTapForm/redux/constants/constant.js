export const ADD_STUDENT_LIST = "ADD_STUDENT_LIST";

export const EMAIL = /^[a-z0-9](\.?[a-z0-9]){3,}@g(oogle)?mail\.com$/g;

export const DELETE_STUDENT = "DELETE_STUDENT";

export const EDIT_STUDENT = "EDIT_STUDENT";

export const SELECT_STUDENT = "SELECT_STUDENT";
