import React, { Component } from "react";
import { connect } from "react-redux";
import { DELETE_STUDENT, SELECT_STUDENT } from "./redux/constants/constant";

class DanhSachQuanLi extends Component {
  state = {
    filter: "",
  };

  render() {
    const { handleXoaSinhVien, handleSelectSinhVien } = this.props;
    const { filter } = this.state;

    return (
      <div>
        <div className="row justify-content-end">
          <div className="col-4  ">
            <input
              type="text"
              className="form-control"
              placeholder="Bạn muốn tìm gì..."
              aria-describedby="helpId"
              value={filter}
              onChange={(event) => {
                this.setState({
                  filter: event.target.value,
                });
              }}
            />
          </div>
        </div>
        <table className="table  mt-5">
          <thead className="bg-dark text-white">
            <tr>
              <th>Mã SV</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.props.danhSachSinhVien
              .filter((x) => (!filter ? true : x.maSV.includes(filter)))
              .map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{item.maSV}</td>
                    <td>{item.hoTen}</td>
                    <td>{item.soDT}</td>
                    <td>{item.email}</td>

                    <td>
                      <button
                        className="btn btn-success mr-3"
                        onClick={() => handleSelectSinhVien(item)}
                      >
                        Sửa
                      </button>
                      <button
                        className="btn btn-danger"
                        onClick={() => handleXoaSinhVien(item)}
                      >
                        Xóa
                      </button>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachSinhVien: state.formReducer.danhSachSinhVien,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleXoaSinhVien: (sinhVien) => {
      dispatch({
        type: DELETE_STUDENT,
        payload: sinhVien,
      });
    },
    handleSelectSinhVien: (sinhVien) => {
      dispatch({
        type: SELECT_STUDENT,
        payload: sinhVien,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DanhSachQuanLi);
