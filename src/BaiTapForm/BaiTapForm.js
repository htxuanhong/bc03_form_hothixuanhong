import React, { Component } from "react";
import DanhSachQuanLi from "./DanhSachQuanLi";
import FormQuanLi from "./FormQuanLi";

export default class BaiTapForm extends Component {
  render() {
    return (
      <div className="container">
        <h3 className="info-h3 bg-dark text-white">Thông tin sinh viên</h3>
        <FormQuanLi />
        <DanhSachQuanLi />
      </div>
    );
  }
}
