import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ADD_STUDENT_LIST,
  EDIT_STUDENT,
  EMAIL,
} from "./redux/constants/constant";

class FormQuanLi extends Component {
  state = {
    isAdd: true,
    maSV: "",
    hoTen: "",
    soDT: "",
    email: "",
    errors: null,
  };

  componentDidUpdate(prevProps, prevState) {
    const { handleRemoveSelectedStudent } = this.props;

    if (
      prevProps.selectedStudent !== this.props.selectedStudent &&
      this.props.selectedStudent
    ) {
      this.setState({
        isAdd: false,
        errors: null,
        ...this.props.selectedStudent,
      });

      handleRemoveSelectedStudent();
    }
  }

  render() {
    const { isAdd, hoTen, maSV, soDT, email, errors } = this.state;

    return (
      <form>
        <div className="row my-4 text-left">
          <div className="col">
            <label>Mã SV</label>
            <input
              type="text"
              name="maSV"
              id="maSV"
              className={`form-control ${errors?.maSV ? "is-invalid" : ""}`}
              placeholder="Nhập mã sinh viên"
              aria-describedby="helpId"
              disabled={!isAdd}
              value={maSV}
              onChange={(event) =>
                this.setState({
                  maSV: event.target.value,
                })
              }
            />
            <div className="invalid-feedback">{errors?.maSV}</div>
          </div>
          <div className="col">
            <label>Họ tên</label>
            <input
              type="text"
              name="hoTen"
              className={`form-control ${errors?.hoTen ? "is-invalid" : ""}`}
              placeholder="Nhập họ & tên"
              aria-describedby="helpId"
              value={hoTen}
              onChange={(event) =>
                this.setState({
                  hoTen: event.target.value,
                })
              }
            />
            <div className="invalid-feedback">{errors?.hoTen}</div>
          </div>
        </div>

        <div className="row my-4 text-left">
          <div className="col">
            <label>Số điện thoại</label>
            <input
              type="number"
              name="soDT"
              className={`form-control ${errors?.soDT ? "is-invalid" : ""}`}
              placeholder="Nhập số điện thoại"
              aria-describedby="helpId"
              value={soDT}
              onChange={(event) =>
                this.setState({
                  soDT: event.target.value,
                })
              }
            />
            <div className="invalid-feedback">{errors?.soDT}</div>
          </div>
          <div className="col">
            <label>Email</label>
            <input
              type="text"
              name="email"
              className={`form-control ${errors?.email ? "is-invalid" : ""}`}
              placeholder="Nhập email"
              aria-describedby="helpId"
              value={email}
              onChange={(event) =>
                this.setState({
                  email: event.target.value,
                })
              }
            />
            <div className="invalid-feedback">{errors?.email}</div>
          </div>
        </div>
        <div className="row justify-content-center mt-5">
          {isAdd ? (
            <div className="col-3">
              <button
                type="button"
                className="btn btn-success btn-add"
                onClick={this._handleSubmit}
              >
                Thêm sinh viên
              </button>
            </div>
          ) : (
            <div className="col-3">
              <button
                type="button"
                className="btn btn-info"
                onClick={this._handleSubmit}
              >
                Cập nhật
              </button>
            </div>
          )}
        </div>
      </form>
    );
  }

  _handleSubmit = (e) => {
    const { handleThemSinhVien, handleSuaSinhVien } = this.props;
    const { isAdd, hoTen, maSV, soDT, email } = this.state;

    const sv = { hoTen, maSV, soDT, email };

    this.setState({
      errors: null,
    });

    const { isValid, errors } = this._validate(sv);

    if (isValid) {
      isAdd
        ? handleThemSinhVien({ hoTen, maSV, soDT, email })
        : handleSuaSinhVien({ hoTen, maSV, soDT, email });

      this.setState({
        isAdd: true,
        hoTen: "",
        maSV: "",
        email: "",
        soDT: "",
      });
    } else {
      this.setState({
        errors,
      });
    }
  };

  _validate = (sv) => {
    const { danhSachSinhVien } = this.props;
    const { isAdd } = this.state;

    let isValid = true;
    let errors = {};

    if (!sv.maSV) {
      errors.maSV = "Vui lòng không để trống!";
      isValid = false;
    }

    if (isAdd && danhSachSinhVien.some((x) => x.maSV === sv.maSV)) {
      errors.maSV = "Mã sinh viên Không được trùng";
      isValid = false;
    }

    if (!sv.hoTen) {
      errors.hoTen = "Vui lòng không để trống!";
      isValid = false;
    }

    if (!sv.soDT) {
      errors.soDT = "Vui lòng không để trống!";
      isValid = false;
    }

    if (!sv.email) {
      errors.email = "Vui lòng không để trống!";
      isValid = false;
    } else if (!EMAIL.test(sv.email)) {
      errors.email = "Email không hợp lệ";
      isValid = false;
    }

    return { isValid, errors };
  };
}

let mapStateToProps = (state) => {
  return {
    selectedStudent: state.formReducer.selectedStudent,
    danhSachSinhVien: state.formReducer.danhSachSinhVien,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleThemSinhVien: (sinhVien) => {
      dispatch({
        type: ADD_STUDENT_LIST,
        payload: sinhVien,
      });
    },
    handleSuaSinhVien: (sinhVien) => {
      dispatch({
        type: EDIT_STUDENT,
        payload: sinhVien,
      });
    },
    handleRemoveSelectedStudent: () => {
      dispatch({
        type: "REMOVE_SELECTED_STUDENT",
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormQuanLi);
